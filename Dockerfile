FROM openjdk:11
VOLUME /tmp
EXPOSE 9090
RUN mkdir -p /app/
RUN mkdir -p /app/logs/
ADD /target/messengerrest-1.0-SNAPSHOT.jar /app/app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar", "/app/app.jar"]