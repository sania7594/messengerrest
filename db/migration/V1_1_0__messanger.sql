--alter table if exists blacklist drop constraint if exists FKj3egsb3xwf3i39l0knubpo0vb;
--alter table if exists blacklistchat drop constraint if exists FK8b4kaefbqywsuh0n91093rdb0;
--alter table if exists listchat drop constraint if exists FKpm15u7cs8plq4yk6s6t2sxfuw;
--alter table if exists messeges drop constraint if exists FK36nocftesyw3f7hsbj91f3pk4;
--drop table if exists blacklist cascade;
--drop table if exists blacklistchat cascade;
--drop table if exists chat cascade;
--drop table if exists listchat cascade;
--drop table if exists messeges cascade;
--drop table if exists reasonblocking cascade;
--drop sequence if exists hibernate_sequence;
--create sequence hibernate_sequence start 1 increment 1;
create table blacklist (
    blacklid int8 not null,
    user_black_list int8,
    reison_id int8, primary key (blacklid));

create table blacklistchat (
    black_list_id int8 not null,
    user_bl_chat int8,
    chat_id int8,
    primary key (black_list_id));
create table chat (
    chat_id int8 not null,
    date TIMESTAMP, owner varchar(255),
    name_chat varchar(255),
    write varchar(255),
    primary key (chat_id));
create table listchat (
    id int8 not null,
    user_list_chat varchar(255),
    chat_id int8, primary key (id));
create table messeges (
    messege_id int8 not null,
    date TIMESTAMP,
    message varchar(255),
    user_messeges int8,
    chat_id int8,
    primary key (messege_id));
create table reasonblocking (
    reison_id int8 not null,
    name_reason varchar(255),
    primary key (reison_id));
alter table if exists blacklist add constraint FKj3egsb3xwf3i39l0knubpo0vb foreign key (reison_id) references reasonblocking;
alter table if exists blacklistchat add constraint FK8b4kaefbqywsuh0n91093rdb0 foreign key (chat_id) references chat;
alter table if exists listchat add constraint FKpm15u7cs8plq4yk6s6t2sxfuw foreign key (chat_id) references chat;
alter table if exists messeges add constraint FK36nocftesyw3f7hsbj91f3pk4 foreign key (chat_id) references chat;