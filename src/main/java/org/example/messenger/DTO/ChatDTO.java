package org.example.messenger.DTO;

import io.swagger.annotations.ApiModelProperty;
import org.example.messenger.entity.BlackListChat;
import org.example.messenger.entity.Chat;
import org.example.messenger.entity.ListChat;
import org.example.messenger.entity.Messeges;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class ChatDTO {

    private Long chatId;
    private String owner;
    private String title;
    private LocalDateTime dateChat;
    private String write;

    private String userIdent;
    private String message;



    private List<UserDTO> listUserDto;


    public ChatDTO(String owner, String title, LocalDateTime dateChat, String write) {
        this.owner = owner;
        this.title = title;
        this.dateChat = dateChat;
        this.write = write;
    }


    public ChatDTO() {

    }

    public ChatDTO(Long chatId, String owner, String title, LocalDateTime dateChat, String write,
                   String userIdent, String message,List<UserDTO> listUserDto) {
        this.chatId = chatId;
        this.owner = owner;
        this.title = title;
        this.dateChat = dateChat;
        this.write = write;
        this.userIdent = userIdent;
        this.message = message;
        this.listUserDto=listUserDto;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getDateChat() {
        return dateChat;
    }

    public void setDateChat(LocalDateTime dateChat) {
        this.dateChat = dateChat;
    }

    public String getWrite() {
        return write;
    }

    public void setWrite(String write) {
        this.write = write;
    }

    public Long getChatId() {
        return chatId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public String getUserIdent() {
        return userIdent;
    }

    public void setUserIdent(String userIdent) {
        this.userIdent = userIdent;
    }

    public List<UserDTO> getListUserDto() {
        return listUserDto;
    }

    public void setListUserDto(List<UserDTO> listUserDto) {
        this.listUserDto = listUserDto;
    }

    public static ChatDTO.Builder newBuilder(){
        return new ChatDTO().new Builder();
    }


    public class Builder {
        private Builder() {
        }

        public ChatDTO.Builder setId(Long chatId){
            ChatDTO.this.chatId=chatId;
            return this;
        }

        public ChatDTO.Builder setOwner(String owner) {
            ChatDTO.this.owner = owner;
            return this;
        }

        public ChatDTO.Builder setNameChat(String title) {
            ChatDTO.this.title = title;
            return this;
        }

        public ChatDTO.Builder setDateChat(LocalDateTime dateChat) {
            ChatDTO.this.dateChat = dateChat;
            return this;
        }

        public ChatDTO.Builder setWrite(String write) {
            ChatDTO.this.write = write;
            return this;
        }

        public ChatDTO.Builder setMessage(String message){
            ChatDTO.this.message=message;
            return this;
        }

        public ChatDTO.Builder setListUserDto(List<UserDTO> listUserDto){
            ChatDTO.this.listUserDto=listUserDto;
            return this;
        }

        public ChatDTO build() {
            return ChatDTO.this;
        }
    }
}
