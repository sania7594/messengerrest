package org.example.messenger.DTO;

import java.sql.Date;

public class FileDTO {
    Long id;
    Date addedDate;
    String userUUID;
    Boolean visibility;
    Long directoryId;
    String directoryPath;
    String filename;
    byte[] fileInBytes;

    public FileDTO(){

    }
    public FileDTO(Long id, Date addedDate, String userUUID, Boolean visibility, Long directoryId, String directoryPath, String filename, byte[] fileInBytes) {
        this.id = id;
        this.addedDate = addedDate;
        this.userUUID = userUUID;
        this.visibility = visibility;
        this.directoryId = directoryId;
        this.directoryPath = directoryPath;
        this.filename = filename;
        this.fileInBytes = fileInBytes;
    }

    public FileDTO(Long id) {
        this.id = id;
    }

    public FileDTO(Date addedDate, String userUUID, Boolean visibility, String directoryPath, String filename, byte[] fileInBytes) {
        this.addedDate = addedDate;
        this.userUUID = userUUID;
        this.visibility = visibility;
        this.directoryPath = directoryPath;
        this.filename = filename;
        this.fileInBytes = fileInBytes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }

    public Long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(Long directoryId) {
        this.directoryId = directoryId;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getFileInBytes() {
        return fileInBytes;
    }

    public void setFileInBytes(byte[] fileInBytes) {
        this.fileInBytes = fileInBytes;
    }
}
