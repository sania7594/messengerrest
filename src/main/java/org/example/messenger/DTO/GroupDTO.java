package org.example.messenger.DTO;

public class GroupDTO {
    private Long groupId;

    private String name;

    public GroupDTO(Long groupId, String name) {
        this.groupId = groupId;
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }
}
