package org.example.messenger.DTO;

import org.example.messenger.entity.Chat;

import java.time.LocalDateTime;
import java.util.Date;

public class UserDTO {
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private Date birthday;
    private String login;
    private Long roleDepartmentDtoList;
    private Long courseSpecialityDtoList;


    public UserDTO(String id, String firstName, String lastName, String email, Date birthday, String login, Long roleDepartmentDtoList, Long courseSpecialityDtoList) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthday = birthday;
        this.login = login;
        this.roleDepartmentDtoList = roleDepartmentDtoList;
        this.courseSpecialityDtoList = courseSpecialityDtoList;
    }

    public UserDTO(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Long getRoleDepartmentDtoList() {
        return roleDepartmentDtoList;
    }

    public void setRoleDepartmentDtoList(Long roleDepartmentDtoList) {
        this.roleDepartmentDtoList = roleDepartmentDtoList;
    }

    public Long getCourseSpecialityDtoList() {
        return courseSpecialityDtoList;
    }

    public void setCourseSpecialityDtoList(Long courseSpecialityDtoList) {
        this.courseSpecialityDtoList = courseSpecialityDtoList;
    }

    public static UserDTO.Builder newBuilder(){
        return new UserDTO().new Builder();
    }

    public class Builder {
        private Builder() {
        }

        public UserDTO.Builder setId(String id){
            UserDTO.this.id=id;
            return this;
        }

        public UserDTO.Builder setFirstName(String firstName) {
            UserDTO.this.firstName = firstName;
            return this;
        }

        public UserDTO.Builder setLastName(String lastName) {
            UserDTO.this.lastName = lastName;
            return this;
        }

        public UserDTO build() {
            return UserDTO.this;
        }
    }
}
