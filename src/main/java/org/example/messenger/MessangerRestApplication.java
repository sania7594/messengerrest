package org.example.messenger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessangerRestApplication {
    public static void main(String[] args) {
        SpringApplication.run(MessangerRestApplication.class, args);
    }
}
