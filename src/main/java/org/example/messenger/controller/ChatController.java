package org.example.messenger.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.example.messenger.DTO.ChatDTO;
import org.example.messenger.entity.Chat;
import org.example.messenger.entity.ChatFile;
import org.example.messenger.entity.ListChat;
import org.example.messenger.entity.Messeges;
import org.example.messenger.service.ChatServiceImpl;
import org.example.messenger.service.FileService;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController()
@RequestMapping("/api/v1/chats")
@Api(value="onlinestore", description="Chats/сhat feedback handler")
public class ChatController {
    ChatServiceImpl chatService;
    FileService fileService;
    private final HttpServletRequest request;
    private String token;
    private String userId;

    private static Logger logger = LoggerFactory.getLogger(ChatController.class);

    @Autowired
    public ChatController(ChatServiceImpl chatService, HttpServletRequest request, FileService fileService){
        this.chatService=chatService;
        this.request = request;
        this.fileService=fileService;
    }

    @ApiOperation(value = "View a list of available chat", response = Chat.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @GetMapping("/{id}")
    public ResponseEntity<Chat> getChat(@PathVariable Long id){
        Chat chat=chatService.oneChat(id);
        return chat != null ?
                new ResponseEntity<>(chat, HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "View a list of available chats", response = Chat.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @GetMapping()
    public ResponseEntity<List<ChatDTO>> getChats(){
        getToken();
        logger.error("Get token: "+getToken());
        List<ChatDTO> chats=chatService.lastChat(getToken());
        return  new ResponseEntity<>(chats,HttpStatus.OK);

    }

    @GetMapping("/{idchat}/files")
    public ResponseEntity<List<ChatFile>> getListChatFile(@PathVariable("idchat") Long idchat){
        List<ChatFile> getChatFiles=fileService.getfilesChat(idchat);
        return new ResponseEntity<>(getChatFiles,HttpStatus.OK);
    }

    private String getToken(){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        token = context.getTokenString();
        AccessToken contextToken = context.getToken();
        userId = contextToken.getSubject();
        return userId;
    }

}
