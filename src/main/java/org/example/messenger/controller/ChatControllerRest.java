package org.example.messenger.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.example.messenger.DTO.ChatDTO;
import org.example.messenger.DTO.FileDTO;
import org.example.messenger.entity.Chat;
import org.example.messenger.entity.Messeges;
import org.example.messenger.service.ChatServiceImpl;
import org.example.messenger.service.FileService;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/v1/chats")
@Api(value="onlinestore", description="Chat сhange сontroller")
public class ChatControllerRest {
    ChatServiceImpl chatService;
    FileService fileService;
    private final HttpServletRequest request;
    private String token;
    private String userId;

    private static Logger logger = LoggerFactory.getLogger(ChatControllerRest.class);


    public ChatControllerRest(ChatServiceImpl chatService, HttpServletRequest request,FileService fileService) {
        this.chatService = chatService;
        this.request = request;
        this.fileService=fileService;
    }


    @ApiOperation(value = "Creating a new message ", response = Chat.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful chat creation"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @PostMapping()
    public ResponseEntity<Chat> createChat(@RequestBody ChatDTO chat){
        getToken();
        logger.error("Get token rest chat"+getToken());
        Chat chatNew=chatService.createChatView(chat);
        return chatNew !=null ?
                new ResponseEntity<>(chatNew, HttpStatus.CREATED):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Delete chat ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful chat delete"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @DeleteMapping("/{id}")
    public ResponseEntity deleteChat(@PathVariable("id") Long id){
        chatService.deleteChat(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/search")
    public ResponseEntity<List<ChatDTO>> getSearchChats(@RequestBody Chat chat){
        List<ChatDTO> chats=chatService.getSearchChats(userId,chat);
        logger.error("UserId Search chat"+ userId);
        return  new ResponseEntity<>(chats,HttpStatus.OK);
    }

    @PostMapping("/{idchat}/files")
    public void addFile(@PathVariable("idchat") Long idChat, @RequestBody Long idFile){
        logger.error("Данные файла"+idFile);
        fileService.createFileChat(idChat,idFile);
    }

    private String getToken(){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        token = context.getTokenString();
        AccessToken contextToken = context.getToken();
        userId = contextToken.getSubject();
        return userId;
    }
}
