package org.example.messenger.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.example.messenger.DTO.UserDTO;
import org.example.messenger.entity.BlackListChat;
import org.example.messenger.entity.Messeges;
import org.example.messenger.service.BlackListChatService;
import org.example.messenger.service.ChatServiceImpl;
import org.example.messenger.service.MessageServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/chats")
@Api(value="onlinestore", description="Messages/message feedback handler")
public class MessageController {
    MessageServiceImpl messageService;
    BlackListChatService blackListChatService;
    private static Logger logger = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    public MessageController(MessageServiceImpl messageService,BlackListChatService blackListChatService){
        this.messageService=messageService;
        this.blackListChatService=blackListChatService;
    }

    @ApiOperation(value = "View a list of available messages", response = Messeges.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @GetMapping("/{idchat}/messages")
    public ResponseEntity<List<Messeges>> getMessages(@PathVariable("idchat") Long idChat){
        List<Messeges> messeges=messageService.lastMessages(idChat);
        return new ResponseEntity<>(messeges, HttpStatus.OK);
    }

    @GetMapping("/{idchat}/users")
    public ResponseEntity<List<UserDTO>> getUserChat(@PathVariable("idchat") Long idChat){
        logger.error("id Chat"+idChat);
        List<UserDTO> userDTOS=messageService.getUserChat(idChat);
        return  new ResponseEntity<>(userDTOS,HttpStatus.OK);
    }

    @GetMapping("/{idchat}/blacklist")
    public ResponseEntity<List<BlackListChat>> getBlackListChat(@PathVariable("idchat") Long idChat){
        List<BlackListChat> blackListChats=blackListChatService.getBlackListChat(idChat);
        return new ResponseEntity<>(blackListChats,HttpStatus.OK);
    }




}
