package org.example.messenger.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.example.messenger.DTO.UserDTO;
import org.example.messenger.entity.Messeges;
import org.example.messenger.service.BlackListChatService;
import org.example.messenger.service.MessageServiceImpl;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/chats")
public class MessageControllerRest {
    private final MessageServiceImpl messageService;
    private final HttpServletRequest request;
    private final BlackListChatService blackListChatService;
    private String token;
    private String userId;

    public MessageControllerRest(MessageServiceImpl messageService, HttpServletRequest request, BlackListChatService blackListChatService) {
        this.messageService = messageService;
        this.request = request;
        this.blackListChatService = blackListChatService;
    }
    @ApiOperation(value = "Creating a new message", response = Messeges.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful message creation"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @PostMapping("/{idchat}/messages")
    public ResponseEntity<Messeges> createMessage(@RequestBody Messeges messeges,@PathVariable("idchat") Long idchat ){
        Messeges messegesNew=messageService.createMessages(messeges,idchat);
        return messegesNew !=null ?
                new ResponseEntity<>(messegesNew, HttpStatus.CREATED):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Delete a specific message")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful message delete"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @DeleteMapping("/{idchat}/messages/{idmessage}")
    public ResponseEntity<Void> deleteMessage(@PathVariable("idmessage") Long idMessage){
        messageService.deleteMessage(idMessage);
        return new ResponseEntity<Void>(HttpStatus.MULTI_STATUS.NO_CONTENT);
    }


    @PutMapping("/{idchat}/messages/{idmessage}")
    public ResponseEntity<Messeges>  putMessage(@PathVariable("idmessage") Long idMessage, @RequestBody Messeges messeges){
        Messeges updateMessage=messageService.updateMessage(idMessage,messeges,getToken());
        return updateMessage != null ?
                new ResponseEntity<>(updateMessage, HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

   @PostMapping("/{idchat}/user")
    public ResponseEntity<UserDTO> addListUSer(@PathVariable("idchat") Long idchat, @RequestBody UserDTO userDTO){
        UserDTO userDTO1=messageService.createListChat(idchat, userDTO);
        return  userDTO1 != null ?
                new ResponseEntity<>(userDTO1, HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/{idchat}/blacklist")
    public ResponseEntity<Void> addUserBlackList(@PathVariable("idchat") Long idchat, @RequestBody UserDTO userDTO){
        blackListChatService.addBlackListChat(idchat,userDTO);
        return new ResponseEntity<Void>(HttpStatus.MULTI_STATUS.NO_CONTENT);
    }

    @PostMapping("/{idchat}/blacklis")
    public ResponseEntity<Void> deleteUserBlackList(@PathVariable("idchat") Long idchat, @RequestBody UserDTO userDTO){
        blackListChatService.deleteBlackListChat(idchat,userDTO);
        return new ResponseEntity<Void>(HttpStatus.MULTI_STATUS.NO_CONTENT);
    }

    private String getToken(){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        token = context.getTokenString();
        AccessToken contextToken = context.getToken();
        userId = contextToken.getSubject();
        return userId;
    }

}
