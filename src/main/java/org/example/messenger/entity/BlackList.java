package org.example.messenger.entity;

import javax.persistence.*;

@Entity
@Table(name="blacklist")
public class BlackList {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long blackLId;
    //user
    @Column
    private Long userBlackList;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="reison_id")
    private ReasonBlocking reasonBlocking;


    public BlackList(Long userBlackList, ReasonBlocking reasonBlocking) {
        this.userBlackList = userBlackList;
        this.reasonBlocking = reasonBlocking;
    }

    public BlackList() {

    }

    public Long getBlackLId() {
        return blackLId;
    }

    public void setBlackLId(Long blackLId) {
        this.blackLId = blackLId;
    }

    public Long getUserBlackList() {
        return userBlackList;
    }

    public void setUserBlackList(Long userBlackList) {
        this.userBlackList = userBlackList;
    }

    public ReasonBlocking getReasonBlocking() {
        return reasonBlocking;
    }

    public void setReasonBlocking(ReasonBlocking reasonBlocking) {
        this.reasonBlocking = reasonBlocking;
    }
}
