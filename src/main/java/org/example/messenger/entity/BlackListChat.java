package org.example.messenger.entity;

import javax.persistence.*;

@Entity
@Table(name="blacklistchat")
public class BlackListChat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long blackListId;
    //user
    @Column
    private String userBlChat;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="chat_id")
    private Chat blackChat;


    public BlackListChat(String userBlChat, Chat blackChat) {
        this.userBlChat = userBlChat;
        this.blackChat = blackChat;
    }

    public BlackListChat() {

    }

    public String getUserBlChat() {
        return userBlChat;
    }

    public void setUserBlChat(String userBlChat) {
        this.userBlChat = userBlChat;
    }

    public Chat getBlackChat() {
        return blackChat;
    }

    public void setBlackChat(Chat blackChat) {
        this.blackChat = blackChat;
    }

}
