package org.example.messenger.entity;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name="chat")
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(notes = "The database generated chat ID")
    private Long chatId;

    //групп владельцев
    @Column
    //private Long owner;
    @ApiModelProperty(notes = "Chat owners")
    private String owner;

    @Column(name="name_chat")
    @ApiModelProperty(notes = "Сhat name")
    private String title;

    @Column(name="date", columnDefinition = "TIMESTAMP")
    @ApiModelProperty(notes = "Date/time the chat was created")
    private LocalDateTime dateChat;

    //private Boolean write;
    //0 and 1
    @ApiModelProperty(notes = "The right to write in the chat")
    private String write;

    @Transient
    private Long userIdent;

    @OneToMany(mappedBy = "chatLK", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<ListChat> listChat;

    @OneToMany(mappedBy = "blackChat", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<BlackListChat> blackChatLK;

    @OneToMany(mappedBy = "chatMessegesLK", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Messeges> chatMesseges;

    @OneToMany(mappedBy = "chatFile", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<ChatFile> chatFiles;

    public Chat(String owner, String title, LocalDateTime dateChat, String write) {
        this.owner = owner;
        this.title = title;
        this.dateChat = dateChat;
        this.write = write;
    }


    public Chat() {

    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getDateChat() {
        return dateChat;
    }

    public void setDateChat(LocalDateTime dateChat) {
        this.dateChat = dateChat;
    }

    public String getWrite() {
        return write;
    }

    public void setWrite(String write) {
        this.write = write;
    }

    public Long getChatId() {
        return chatId;
    }

    public static Chat.Builder newBuilder(){
        return new Chat().new Builder();
    }


    public class Builder {
        private Builder() {
        }

        public Chat.Builder setId(Long chatId){
            Chat.this.chatId=chatId;
            return this;
        }

        public Chat.Builder setOwner(String owner) {
            Chat.this.owner = owner;
            return this;
        }

        public Chat.Builder setNameChat(String title) {
            Chat.this.title = title;
            return this;
        }

        public Chat.Builder setDateChat(LocalDateTime dateChat) {
            Chat.this.dateChat = dateChat;
            return this;
        }

        public Chat.Builder setWrite(String write) {
            Chat.this.write = write;
            return this;
        }

        public Chat build() {
            return Chat.this;
        }
    }
}
