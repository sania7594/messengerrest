package org.example.messenger.entity;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name="chat_file")
public class ChatFile {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(notes = "The database generated chat ID")
    private Long chatFileId;

    @Column
    Long idFile;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="chat_id")
    private Chat chatFile;

    public ChatFile(){

    }


    public ChatFile(Long idFile, Chat chatFile) {
        this.idFile = idFile;
        this.chatFile = chatFile;
    }

    public Long getIdFile() {
        return idFile;
    }

    public void setIdFile(Long idFile) {
        this.idFile = idFile;
    }

    public Chat getChatFile() {
        return chatFile;
    }

    public void setChatFile(Chat chatFile) {
        this.chatFile = chatFile;
    }
}
