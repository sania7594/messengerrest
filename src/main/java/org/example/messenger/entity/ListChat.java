package org.example.messenger.entity;


import javax.persistence.*;

@Entity
@Table(name="listchat")
public class ListChat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;




    @Column
    private String userListChat;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="chat_id")
    private Chat chatLK;

    public ListChat(String userListChat, Chat chatLK) {
        this.userListChat = userListChat;
        this.chatLK = chatLK;
    }

    public ListChat() {

    }

    public String getUserListChat() {
        return userListChat;
    }

    public void setUserListChat(String userListChat) {
        this.userListChat = userListChat;
    }


    public Chat getChatLK() {
        return chatLK;
    }

    public void setChatLK(Chat chatLK) {
        this.chatLK = chatLK;
    }
}
