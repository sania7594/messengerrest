package org.example.messenger.entity;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;

@Entity
@Table(name="messeges")
public class Messeges {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(notes = "The database generated message ID")
    private Long messegeId;

    //user
    @Column
    @ApiModelProperty(notes = "ID of the user who created the message")
    private String userMesseges;

    @Column(name="date", columnDefinition = "TIMESTAMP")
    @ApiModelProperty(notes = "Date/time the chat was created")
    private LocalDateTime date;

    @Column
    @ApiModelProperty(notes = "Message body")
    private String message;


    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="chat_id")
    @ApiModelProperty(notes = "Сhat stored in the database")
    private Chat chatMessegesLK;


    public Messeges(String userMesseges, LocalDateTime date, String message, Chat chatMessegesLK) {
        this.userMesseges = userMesseges;
        this.date = date;
        this.message = message;
        this.chatMessegesLK = chatMessegesLK;
    }



    public Messeges() {

    }

    public Long getMessegeId() {
        return messegeId;
    }

    public void setMessegeId(Long messegeId) {
        this.messegeId = messegeId;
    }

    public String getUserMessegesLK() {
        return userMesseges;
    }

    public void setUserMessegesLK(String userMesseges) {
        this.userMesseges = userMesseges;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String body) {
        this.message = body;
    }

    public Chat getChatMessegesLK() {
        return chatMessegesLK;
    }

    public void setChatMessegesLK(Chat chatMessegesLK) {
        this.chatMessegesLK = chatMessegesLK;
    }

    public static Builder newBuilder(){
        return new Messeges().new Builder();
    }



    @Override
    public String toString() {
        return "Messeges{" +
                "date=" + date +
                ", body='" + message + '\'' +
                '}';
    }

    public class Builder{
        private Builder(){}

        public Builder setId(Long messegesId){
            Messeges.this.messegeId =messegesId;
            return this;
        }

        public Builder setUserId(String userMesseges){
            Messeges.this.userMesseges=userMesseges;
            return this;
        }

        public Builder setMessege(String messege){
            Messeges.this.message =messege;
            return this;
        }

        public Builder setData(LocalDateTime date){
            Messeges.this.date=date;
            return this;
        }

        public Builder userMessagerLK(String userMesseges){
            Messeges.this.userMesseges=userMesseges;
            return this;
        }

        public Builder setChatLK(Chat chatMessegesLK){
            Messeges.this.chatMessegesLK=chatMessegesLK;
            return this;
        }

        public Messeges build(){
            return Messeges.this;
        }

    }

}

