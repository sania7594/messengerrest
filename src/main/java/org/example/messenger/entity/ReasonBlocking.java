package org.example.messenger.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="reasonblocking")
public class ReasonBlocking {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long reisonId;

    @Column
    private String nameReason;

    @OneToMany(mappedBy = "reasonBlocking",cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    private Set<BlackList> blackList;

    public ReasonBlocking(String nameReason) {
        this.nameReason = nameReason;
    }

    public ReasonBlocking() {

    }

    public String getNameReason() {
        return nameReason;
    }

    public void setNameReason(String nameReason) {
        this.nameReason = nameReason;
    }
}
