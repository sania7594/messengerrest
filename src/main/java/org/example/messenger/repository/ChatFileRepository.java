package org.example.messenger.repository;

import org.example.messenger.entity.BlackListChat;
import org.example.messenger.entity.Chat;
import org.example.messenger.entity.ChatFile;
import org.example.messenger.entity.ListChat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatFileRepository extends JpaRepository<ChatFile,Long> {
    List<ChatFile> findAllByChatFile(Chat chat);
}
