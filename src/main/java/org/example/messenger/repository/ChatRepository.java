package org.example.messenger.repository;

import org.example.messenger.entity.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ChatRepository extends JpaRepository<Chat,Long> {
    Chat findByChatId(Long id);
    @Query(value="SELECT * FROM chat ORDER BY chat_id DESC LIMIT 10", nativeQuery = true)
    List<Chat> findLast10Chat();

    Chat findByDateChatAndTitle(LocalDateTime date, String title);

}
