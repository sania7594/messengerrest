package org.example.messenger.repository;

import org.example.messenger.entity.Chat;
import org.example.messenger.entity.ListChat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListChatRepository extends JpaRepository<ListChat, Long> {
    //@Query("select  z.userlistchat, z.chat_id from listchat z  where z.chat_id =?1")
    //List<ListChat> getChatIdList(Chat chat);

    List<ListChat> findAllByChatLK(Chat chat);

    ListChat findByUserListChatAndChatLK(String user,Chat chat);


}
