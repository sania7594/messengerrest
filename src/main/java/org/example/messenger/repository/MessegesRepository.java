package org.example.messenger.repository;

import org.example.messenger.entity.Chat;
import org.example.messenger.entity.Messeges;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface MessegesRepository extends JpaRepository<Messeges, Long> {
    Messeges findByMessegeId(Long id);

    List<Messeges> findByChatMessegesLK(Chat id);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value="UPDATE messeges  set message=:message where messege_id=:messegeId",nativeQuery = true)
    void updateMesseges(@Param("message") String message,@Param("messegeId") Long messegeId);


    Messeges findFirstByChatMessegesLKOrderByDateDesc(Chat chat);


}
