package org.example.messenger.service;

import org.example.messenger.DTO.UserDTO;
import org.example.messenger.entity.BlackListChat;
import org.example.messenger.entity.Chat;
import org.example.messenger.entity.ListChat;
import org.example.messenger.repository.BlackListChatRepository;
import org.example.messenger.repository.ChatRepository;
import org.example.messenger.repository.ListChatRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlackListChatService {
    private final BlackListChatRepository blackListChatRepository;
    private final ChatRepository chatRepository;
    private final ListChatRepository listChatRepository;
    private static Logger logger = LoggerFactory.getLogger(ChatServiceImpl.class);

    @Autowired
    public BlackListChatService(BlackListChatRepository blackListChatRepository, ChatRepository chatRepository, ListChatRepository listChatRepository) {
        this.blackListChatRepository = blackListChatRepository;
        this.chatRepository = chatRepository;
        this.listChatRepository = listChatRepository;
    }

    public List<BlackListChat> getBlackListChat(Long idChat){
        Chat chat=chatRepository.findByChatId(idChat);
        return blackListChatRepository.findAllByBlackChat(chat);
    }

    public void addBlackListChat(Long id, UserDTO userDTO){
        Chat chat=chatRepository.findByChatId(id);
        String idUser=userDTO.getId();
        BlackListChat blackListChat=new BlackListChat(idUser,chat);
        ListChat listChat= listChatRepository.findByUserListChatAndChatLK(idUser,chat);
        listChatRepository.delete(listChat);
        blackListChatRepository.save(blackListChat);
    }

    public void deleteBlackListChat(Long idchat,UserDTO userDTO){
        Chat chat=chatRepository.findByChatId(idchat);
        String idUser=userDTO.getId();
        BlackListChat blackListChat=new BlackListChat(idUser,chat);
        ListChat listChat= listChatRepository.findByUserListChatAndChatLK(idUser,chat);
        listChatRepository.save(listChat);
        blackListChatRepository.delete(blackListChat);
    }
}
