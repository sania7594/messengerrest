package org.example.messenger.service;

import org.example.messenger.DTO.ChatDTO;
import org.example.messenger.entity.Chat;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ChatService {

    List<ChatDTO> lastChat(String userId);
    Chat oneChat(Long idChat);
    Chat createChatView(ChatDTO chat);
    void deleteChat(Long chatId);

}
