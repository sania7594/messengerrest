package org.example.messenger.service;

import org.example.messenger.DTO.ChatDTO;
import org.example.messenger.DTO.UserDTO;
import org.example.messenger.controller.ChatController;
import org.example.messenger.entity.Chat;
import org.example.messenger.entity.ListChat;
import org.example.messenger.entity.Messeges;
import org.example.messenger.repository.ChatRepository;
import org.example.messenger.repository.ListChatRepository;
import org.example.messenger.repository.MessegesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ChatServiceImpl implements ChatService {
    private final ChatRepository chatRepository;
    MessegesRepository messegesRepository;
    private final ListChatRepository listChatRepository;

    private static Logger logger = LoggerFactory.getLogger(ChatServiceImpl.class);

    @Autowired
    public ChatServiceImpl(ChatRepository chatRepository, ListChatRepository listChatRepository,
                           MessegesRepository messegesRepository) {
        this.chatRepository = chatRepository;
        this.listChatRepository=listChatRepository;
        this.messegesRepository=messegesRepository;
    }

    public List<ChatDTO> lastChat(String userId){
        List<Chat> resultChat=new ArrayList<>();
        List<ListChat> chatRepositories=listChatRepository.findAll();
        List<ChatDTO> chatsDTO=new ArrayList<>();

        //список всех чатов где есть юзер
        for (ListChat listChat:chatRepositories){
            String userIdLC=listChat.getUserListChat();
            if (userIdLC.equals(userId)){
                //resultListChat.add(listChat);
                resultChat.add(listChat.getChatLK());
            }
        }

        for (Chat chat:resultChat){
            Long chatId=chat.getChatId();
            String title=chat.getTitle();
            LocalDateTime dateChat=chat.getDateChat();
            Chat chatMessegesLK=chatRepository.findByChatId(chatId);
            Messeges message=messegesRepository.findFirstByChatMessegesLKOrderByDateDesc(chatMessegesLK);

            if (message==null){
                String messageСheck="Список сообщений в беседе пуст";
                ChatDTO chatDTO=chatLastMessage(chatId,title,dateChat,messageСheck);
                chatsDTO.add(chatDTO);
            }
            else{
                String messageСheck=message.getMessage();
                ChatDTO chatDTO=chatLastMessage(chatId,title,dateChat,messageСheck);
                chatsDTO.add(chatDTO);
            }

        }

        return chatsDTO;
    }

    /*
    * Создать чат с последним сообщением
    * */
    private ChatDTO chatLastMessage(Long chatId,String title,LocalDateTime dateChat,String messageСheck){
        return ChatDTO.newBuilder().setId(chatId).setNameChat(title).setDateChat(dateChat)
                .setMessage(messageСheck).build();
    }


    public Chat oneChat(Long idChat){
        return chatRepository.findByChatId(idChat);
    }

    /*
    Создание чата
    */
    public Chat createChatView(ChatDTO chat){
        String owner=chat.getOwner();
        String nameChat=chat.getTitle();
        LocalDateTime date=LocalDateTime.now();
        String write=chat.getWrite();
        String userId= chat.getUserIdent();
        List<UserDTO> listUserDto=chat.getListUserDto();
        Chat chatConvertDTO=new Chat(owner,nameChat,date,write);
        Chat proverka=(nameChat ==null || nameChat.isEmpty())?chatConvertDTO:createNewChat(owner,nameChat,date,write);
        Chat chatBD=searchСhat(proverka);
        return chatView(chatBD,userId,listUserDto);
        //return proverka;
    }

    /*
    * Поиск чатов по названию пользователем
    * */
    public List<ChatDTO> getSearchChats(String userId, Chat chat){
        String title=chat.getTitle();
        List<ChatDTO>  chatsWithUserser=lastChat(userId);
        List<ChatDTO> resultSearchChat = new ArrayList<>();
        String upperTitle=title.toUpperCase();
        String lowerTitle=title.toLowerCase();
        String oneBig = title.substring(0, 1).toUpperCase() + title.substring(1); //первая большая

        for (ChatDTO chatSearch :chatsWithUserser) {
            if (chatSearch.getTitle().equals(title) || chatSearch.getTitle().equals(lowerTitle) || chatSearch.getTitle().equals(upperTitle) || chatSearch.getTitle().equals(oneBig)) {
                ChatDTO convertChat=addChatProperty(chatSearch);
                resultSearchChat.add(convertChat);
            }
        }
        return resultSearchChat;
    }


    /*
    Поиск чата в бд по названию и дате(сокращение вероятности появления 2 объектов)
    * */

    private Chat searchСhat(Chat chat){
        LocalDateTime date=chat.getDateChat();
        String nameChat=chat.getTitle();
        return chatRepository.findByDateChatAndTitle(date,nameChat);
    }
    /*
     * Формирование объекта на отдачу на фронт
     * */
    //write убери если не нужен
    private Chat chatView(Chat chat,String userId,List<UserDTO> listUserDto){
        Long chatID=chat.getChatId();
        String nameChat=chat.getTitle();
        LocalDateTime date=chat.getDateChat();
        String write=chat.getWrite();
        createListChat(chatID,userId,listUserDto);
        Chat resultChat= Chat.newBuilder().setId(chatID).setNameChat(nameChat).setDateChat(date).setWrite(write).build();
        return resultChat;
    }

    /*
     * создаем новый чат
     * */
    private Chat createNewChat(String owner,String nameChat, LocalDateTime date,String write) {
        Chat newChat= Chat.newBuilder().setOwner(owner).setNameChat(nameChat).setDateChat(date).setWrite(write).build();
        return chatRepository.save(newChat);
    }

    /*Конвектор из String в Long
    * */
    private Long convectorLong(String id){
        return Long.valueOf(id).longValue();
    }

    /*создание нового списка чатов
    * */
    private void createListChat(Long chatId, String userId, List<UserDTO> listUserDto){
        Chat chat=chatRepository.findByChatId(chatId);
        for (UserDTO userDTO: listUserDto){
            String idUser=userDTO.getId();
            ListChat listChat=new ListChat(idUser,chat);
            listChatRepository.save(listChat);
        }
    }

    /*
    * Добавление свойства чата
    * */
    private ChatDTO addChatProperty(ChatDTO chat){
        Long chatID=chat.getChatId();
        String nameChat=chat.getTitle();
        LocalDateTime date=chat.getDateChat();
        String write=chat.getWrite();
        String message=chat.getMessage();
        ChatDTO resultChat= ChatDTO.newBuilder().setId(chatID).setNameChat(nameChat).setDateChat(date).setWrite(write).setMessage(message).build();
        return resultChat;
    }

    /*
    * получение пользователя
    * */
    //private UserDTO getUser()

    /* Удаление чата

     */
    public void deleteChat(Long chatId){
        Chat chat=chatRepository.findByChatId(chatId);
        chatRepository.delete(chat);
    }



}
