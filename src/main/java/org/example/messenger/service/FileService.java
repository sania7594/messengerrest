package org.example.messenger.service;

import org.example.messenger.DTO.FileDTO;
import org.example.messenger.entity.Chat;
import org.example.messenger.entity.ChatFile;
import org.example.messenger.entity.ListChat;
import org.example.messenger.repository.ChatFileRepository;
import org.example.messenger.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileService {
    private final ChatFileRepository chatFileRepository;
    private final ChatRepository chatRepository;

    @Autowired
    public FileService(ChatFileRepository chatFileRepository, ChatRepository chatRepository) {
        this.chatFileRepository = chatFileRepository;
        this.chatRepository = chatRepository;
    }

    public void createFileChat(Long idChat, Long idFile){
        Chat chat=chatRepository.findByChatId(idChat);
        ChatFile chatFile=new ChatFile(idFile,chat);
        chatFileRepository.save(chatFile);
    }

    public List<ChatFile> getfilesChat(Long idchat){
        Chat chat=chatRepository.findByChatId(idchat);
        return chatFileRepository.findAllByChatFile(chat);
    }
}
