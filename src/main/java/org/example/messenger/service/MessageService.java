package org.example.messenger.service;

import org.example.messenger.entity.Messeges;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MessageService {

    List<Messeges> lastMessages(Long idChat);
    Messeges createMessages(Messeges messeges,Long idChat);
    void deleteMessage(Long message);
    Messeges updateMessage(Long idMessage,Messeges messeges, String userId);

}
