package org.example.messenger.service;

import org.example.messenger.DTO.UserDTO;
import org.example.messenger.entity.Chat;
import org.example.messenger.entity.ListChat;
import org.example.messenger.entity.Messeges;
import org.example.messenger.repository.ChatRepository;
import org.example.messenger.repository.ListChatRepository;
import org.example.messenger.repository.MessegesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MessageServiceImpl implements MessageService{

    private final MessegesRepository messegesRepository;
    private final ChatRepository chatRepository;
    private final ListChatRepository listChatRepository;
    private static Logger logger = LoggerFactory.getLogger(ChatServiceImpl.class);


    @Autowired
    public MessageServiceImpl(MessegesRepository messegesRepository, ChatRepository chatRepository, ListChatRepository listChatRepository){
        this.messegesRepository=messegesRepository;
        this.chatRepository=chatRepository;
        this.listChatRepository = listChatRepository;
    }


    //chat_id
    public List<Messeges> lastMessages(Long idChat){
        Chat chat=chatRepository.findByChatId(idChat);
        List<Messeges> listMessage=messegesRepository.findByChatMessegesLK(chat);
        return listMessage;
    }

    public Messeges createMessages(Messeges messeges,Long idChat){
        String string=messeges.getMessage();
        String idUser=messeges.getUserMessegesLK();
        LocalDateTime date=LocalDateTime.now();
        Chat chat=chatRepository.findByChatId(idChat) ;
        Messeges message=Messeges.newBuilder().setUserId(idUser).setMessege(string).setData(date).setChatLK(chat).build();
        messegesRepository.save(message);
        Long messegeId=message.getMessegeId();

        Messeges resultMessages=Messeges.newBuilder().setId(messegeId).setUserId(idUser).setMessege(string).setData(date).setChatLK(chat).build();
        return resultMessages;
    }

    /*Конвектор из String в Long
     * */
    private Long convectorLong(String id){
        return Long.valueOf(id).longValue();
    }

    /*создание нового списка чатов*/

    public void deleteMessage(Long message){
        Messeges newMessages=messegesRepository.findByMessegeId(message);
        messegesRepository.delete(newMessages);
    }

    /*
       Обновление объекта
     */
    public Messeges updateMessage(Long idMessage,Messeges messeges, String userId){
        Messeges originalMessage=messegesRepository.findByMessegeId(idMessage);
        String message=messeges.getMessage();
        messegesRepository.updateMesseges(message,idMessage);
        Messeges updateMessage=messegesRepository.findByMessegeId(idMessage);
        Long messegeId=updateMessage.getMessegeId();
        String string=updateMessage.getMessage();
        LocalDateTime date=updateMessage.getDate();
        Chat chat=updateMessage.getChatMessegesLK();
        Messeges resultMessages=Messeges.newBuilder().setId(messegeId).setUserId(userId).setMessege(string).setData(date).setChatLK(chat).build();

        return resultMessages;
    }

    //отдаем id пользователей
    public List<UserDTO> getUserChat(Long idChat){
        List<UserDTO> userDTOS=new ArrayList<>();

        Chat chat=chatRepository.findByChatId(idChat);
        List<ListChat>  chatRepositorys=listChatRepository.findAllByChatLK(chat);

        for (ListChat listChat:chatRepositorys){
                String idUser=listChat.getUserListChat();
                Chat chatv=listChat.getChatLK();

                UserDTO userDTO=UserDTO.newBuilder().setId(idUser).build();
                userDTOS.add(userDTO);
        }

        return userDTOS;
    }


    public UserDTO createListChat(Long idChat, UserDTO userDTO){
        String id= userDTO.getId();
        Chat chat=chatRepository.findByChatId(idChat);
        ListChat listChat=new ListChat(id,chat);
        listChatRepository.save(listChat);
        return userDTO;
    }
}
